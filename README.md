##Pełnomocnik (wzorzec projektowy)
**Pełnomocnik(Proxy)** - jest wzorem strukturalnym, który pełni rolę reprezentanta innego obiektu w celu uzyskania nadzorowanego dostępu do obiektu, który przedstawia.

---
## Cele

1.Kontroluje dostęp do klas i jej obiektów, pośrednio poprzez inną klasę i obiekty.
2.Tworzy opakowanie dla klas, żeby je chronić przez nadmiernym skomplikowaniem.
3.Tworzy obiekty(zwykle te które zżerają dużo zasobów procesora) wtedy kiedy są one naprawdę potrzebne.

---

## Zastosowanie

1.Kontrola dostępu do ochranianego obiektu – np. dostęp do danych po autoryzacji użytkownika
2.Opóźnienie tworzenia kosztownego obiektu, obiekt jest tworzony kiedy to jest niezbędne – tworzenie na żądanie
3.Emulacja zdalnego obiektu, np.: dane obiektu przesyłane są przez internet
4.Cachowanie pewnych danych obiektu – niektóre dane klasy długo się generują, albo są przeliczane wcześniej
5.Dodatkowe operacje wykonywane przy dostępie do obiektu np.: zliczanie ilości referencji, czy tzw. Sprytne wskaźniki

---

## Założenia


1.Pełnomocnik wirtualny - odraczanie kosztów związanych z ewentualnym tworzeniem „grubego” obiektu (por. leniwa inicjacja)
Zdalny pełnomocnik obiektu z innego procesu lub komp.
2.Pośrednik zabezpieczający – celem jest zabezpieczenie obiektu lub programu (sprawdzanie blokad), 
a nie ograniczenie użycia zasobów
3.Inteligentne wskaźniki (referencje) – realizują także powyższe cele
4.Pośrednik danych – częsty przykład, unifikacja

---

## Implementacja

Stworzymy interfejs Image i konkretne klasy implementujące interfejs Image. ProxyImage jest klasą proxy, która redukuje rozmiar pamięci ładowanego obiektu RealImage.
ProxyPatternDemo, nasza klasa demo, użyje ProxyImage, aby załadować i wyświetlić obiekt Image zgodnie z potrzebami.

---

## Źródło

https://czub.info/2015/wzorzec-proxy-pelnomocnik/
http://devman.pl/pl/techniki/wzorce-projektowe-pelnomocnikproxy/
